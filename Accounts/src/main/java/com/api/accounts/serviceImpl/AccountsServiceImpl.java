package com.api.accounts.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.accounts.model.Accounts;
import com.api.accounts.repo.IAccountsRepo;
import com.api.accounts.services.IAccountsService;

@Service
public class AccountsServiceImpl implements IAccountsService {

	
	@Autowired
	private IAccountsRepo repo;
	
	
	@Override
	public List<Accounts> listar() throws Exception {
		
		return repo.findAll();
	}

	@Override
	public Accounts listarPorId(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
