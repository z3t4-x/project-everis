package com.api.accounts.services;

import java.util.List;

import com.api.accounts.model.Accounts;

public interface IAccountService  {

	Accounts registrar(Accounts a)throws Exception;
	Accounts modificar(Accounts a)throws Exception;
	List<Accounts> listar()throws Exception;
	Accounts listarPorId(String id)throws Exception;
	
}
