package com.orquestador.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Persons {

	 private String id;
	 private String document;
	 private boolean fingerprint;
	 private boolean blacklist;

}
