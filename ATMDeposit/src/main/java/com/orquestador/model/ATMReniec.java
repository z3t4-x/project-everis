package com.orquestador.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class ATMReniec {
	
	@Id
	private String id;
	private String document;
	private String entityname;
	private boolean success;
}
