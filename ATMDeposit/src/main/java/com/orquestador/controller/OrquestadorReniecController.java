package com.orquestador.controller;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orquestador.model.ATMReniec;
import com.orquestador.services.OrquestadorApiReniec;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/external")
public class OrquestadorReniecController {

	
	
    @GetMapping(
            value = "/reniec/{validate}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public long examProcess3() {
        long start = System.currentTimeMillis();
        

        return System.currentTimeMillis()-start;
    }
}
