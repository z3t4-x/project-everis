package com.api.fingerprints.services;

import java.util.List;

import com.api.fingerprints.model.Fingerprints;

public interface IFingerprintService {

	
	Fingerprints registrar(Fingerprints f) throws Exception;
	Fingerprints modificar(Fingerprints f) throws Exception;
	List<Fingerprints> listar() throws Exception;
	Fingerprints listarPorId(String id) throws Exception;
	Fingerprints listarPorDoc(String doc) throws Exception;
}
