package com.api.services;

import java.util.List;

import com.api.cards.model.Cards;

public interface ICardService  {

	
	Cards registrar(Cards c);
	Cards modificar(Cards c);
	List<Cards> listar();
	Cards listarPorId(String id);
}
