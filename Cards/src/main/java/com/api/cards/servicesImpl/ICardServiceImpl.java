package com.api.cards.servicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.cards.model.Cards;
import com.api.cards.repo.ICardRepo;
import com.api.services.ICardService;

@Service
public class ICardServiceImpl implements ICardService{

	
	@Autowired
	private ICardRepo repo;
	
	@Override
	public Cards registrar(Cards c) {
		return repo.save(c);
	}

	@Override
	public Cards modificar(Cards c) {
		
		return repo.save(c);
	}

	@Override
	public List<Cards> listar() {
		
		return repo.findAll();
	}

	@Override
	public Cards listarPorId(String id) {
		Optional<Cards> op =  repo.findById(id);
		return op.isPresent() ? op.get() : new Cards();
	}

}
