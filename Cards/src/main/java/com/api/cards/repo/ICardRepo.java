package com.api.cards.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.api.cards.model.Cards;

public interface ICardRepo extends MongoRepository<Cards, String> {

}
