package com.api.cards.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Cards {

	
	@Id
	private String carNumber;
	private boolean active;
}
